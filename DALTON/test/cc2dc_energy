#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi

#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > cc2dc_energy.info <<%EOF%
   cc2dc_energy  
   --------------
   Molecule:         H2O in a spherical cavity of radius 4.00 au
   Wave Function:    CC2 / cc-pVDZ
   Test Purpose:     Check energy, dipole moment quadrupole moment and
                     second order electric moment.
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > cc2dc_energy.mol <<%EOF%
BASIS
cc-pVDZ
H2O i H2O(DC)
------------------------
    2    0         1 1.00D-12
        8.0   1
O            0.000000        0.000000        0.000000
        1.0   2
H           -0.756799        0.000000        0.586007
H            0.756799        0.000000        0.586007
%EOF%
#
#######################################################################
#  DALTON INPUT
#######################################################################
cat > cc2dc_energy.dal <<%EOF%
**DALTON INPUT
.RUN WAVEFUNCTION
**INTEGRALS
.DIPLEN
.NUCPOT
.NELFLD
.THETA
.SECMOM
*ONEINT
.SOLVENT
 10
**WAVE FUNCTIONS
.CC
*SCF INPUT
.THRESH
1.0D-11
*CC INP
.CC2
.THRLEQ
 1.0D-9
.THRENR
 1.0D-9
.MAX IT
 90
.MXLRV
 180
*CCSLV
.SOLVAT
 1
10 4.00  78.54  1.778
.ETOLSL
 1.0D-7
.TTOLSL
 1.0D-7
.LTOLSL
 1.0D-7
.MXSLIT
 200
*CCFOP
.DIPMOM
.QUADRU
.SECMOM
.NONREL
**END OF DALTON INPUT
%EOF%
#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >cc2dc_energy.check
cat >>cc2dc_energy.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

if $GREP -q "not implemented for parallel calculations" $log; then
   echo "TEST ENDED AS EXPECTED"
   exit 0
fi

# Total energy and solvation energy compared:
CRIT1=`$GREP "CC2      Total energy:                 -76\.2386471." $log | wc -l`
CRIT2=`$GREP "CC2      Solvation energy:              ( \-|\-0)\.0076651." $log | wc -l`
TEST[1]=`expr $CRIT1 \+ $CRIT2`
CTRL[1]=2
ERROR[1]="ENERGY TERMS NOT CORRECT"

# Dipole moment components compared:
CRIT1=`$GREP "z * ( |0)\.8459608. * 2\.150217.. * 7\.172354.." $log | wc -l`
TEST[2]=`expr $CRIT1`
CTRL[2]=1
ERROR[2]="DIPOLE MOMENT NOT CORRECT"

# Quadrupole moment components compared:
CRIT1=`$GREP "1 * 1\.5620040. * (  | 0| \-|\-0)\.00000000 * (  | 0| \-|\-0)\.00000000" $log | wc -l`
CRIT2=`$GREP "2 * (  | 0| \-|\-0)\.00000000 * -1\.706191.. * (  | 0| \-|\-0)\.00000000" $log | wc -l`
CRIT3=`$GREP "3 * (  | 0| \-|\-0)\.00000000 * (  | 0| \-|\-0)\.00000000 * ( |0)\.1441875." $log | wc -l`
TEST[3]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[3]=3
ERROR[3]="QUADRUPOLE MOMENT NOT CORRECT"

# Second order electric moment components compared:
CRIT1=`$GREP "1 * 7\.1839465. * (  | 0| \-|\-0)\.00000000 * (  | 0| \-|\-0)\.00000000" $log | wc -l`
CRIT2=`$GREP "2 * (  | 0| \-|\-0)\.00000000 * 5\.2721265. * (  | 0| \-|\-0)\.00000000" $log | wc -l`
CRIT3=`$GREP "3 * (  | 0| \-|\-0)\.00000000 * (  | 0| \-|\-0)\.00000000 * 6\.4911778." $log | wc -l`
TEST[4]=`expr $CRIT1 \+ $CRIT2 \+ $CRIT3`
CTRL[4]=3
ERROR[4]="SECOND ORDER MOMENT NOT CORRECT"

PASSED=1
for i in 1 2 3 4
do 
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo "${ERROR[i]} ( test = ${TEST[i]}; control = ${CTRL[i]} ); "
     PASSED=0
   fi
done 

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM 
  exit 1
fi

%EOF%
#######################################################################
